CREATE SCHEMA if not exists S_DWH_COM;

create or replace VIEW S_DWH_COM.V_MD_CUSTOMER as select * from s_raw_kafka.customer;
create or replace VIEW S_DWH_COM.V_MD_NATION   as select * from s_raw_kafka.nation;
create or replace VIEW S_DWH_COM.V_MD_REGION   as select * from s_raw_kafka.region;
create or replace VIEW S_DWH_COM.V_MD_SUPPLIER as select * from s_raw_kafka.supplier;


